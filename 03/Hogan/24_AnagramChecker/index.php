<?php
    /* Anagram Checker
     *
     * Brug count_chars() til at tælle karakterer med. count_chars returnerer et array eller en string afhængig af mode.
     */

    $firstString = $_GET["firstString"];
    $secondString = $_GET["secondString"];

    function isAnagram($firstString, $secondString){
        return count_chars($firstString, 1) == count_chars($secondString, 1);
    }
    echo "Enter two strings and i'll tell you if they are anagrams<br>";
    echo "Enter the first string: " . $firstString . "<br>";
    echo "Enter the second string: " . $secondString . "<br>";
    if(isAnagram($firstString, $secondString)){
        echo $firstString . " and " . $secondString . " are anagrams";
    };

?>