<?
    /*
     * 01_02 - Variabler
     * I javascript deklarerede vi variabler sålede "var navn = ". I PHP deklarerer vi variabler således "$navn"
     */

    //Simple variabler af typen string og number (integer)   
    $firstName = "Klaus";
    $lastName = "Nørregaard";
    $phoneNum = 41770410;
    $email = "kl@eadania.dk";
    $officeHours = "8.10 - 15.30";

    echo $firstName . " " . $lastName;

    //* Øvelse 01_02 *// 
        // Output: 
        // Mit navn er [firstName] [lastname]. 
        // Jeg kan træffes i kontortiden [officeHours] på telefon [phoneNum] eller email [email]
?>