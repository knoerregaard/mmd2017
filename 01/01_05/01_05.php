<?php
    /*
    * Autoincrement og decrement
    * ++ og --
    */

    $animals = array('And','Gås','Svane','Spurv','Pade','Ål');
    
    //Vi deklarerer en tæller med værdien 0.
    $counter = 0;

    //Hvor hvor mange dyr er set i søen. Vi benytter foreach til at løbe igennem arrayet.
    foreach ($animals as $animal) {
        $counter++;
    }
    //Og så udskriver vi tællerens værdi.
    echo "Der er " . $counter . " dyr i søen";
    echo "<br>";

    //Vi kan også dekremente med -- operatoren.

    foreach ($animals as $animal) {
        $counter--;
    }

    //Hvor mange er der så?
    echo "Der er " . $counter . " dyr i søen";
    echo "<br>";
    
    //* Øvelse *//
    // Hvis vi ønsker, at er ikke må være mindre end 2 dyr i søen, hvad gør vi så?

?>
