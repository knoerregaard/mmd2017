<?php
    /*
    * Emnet for dagen er løkkekonstruktioner, men vi skal lige have gang i $_GET allerførst
    * Forms elementet benytter vi når der skal sendes data til og fra en central server.
    * Når forms elementet har en metode-attribut med værdien GET, så sendes data via sidens URL.
    */
    $firstName = "Klaus";           //$_GET[firstName]
    $lastName = "Nørregaard";       //$_GET[lastName]
    $phoneNum = 41770410;           //$_GET[phoneNum]
    $email = "kl@eadania.dk";       //$_GET[email]
    $officeHours = "8.10 - 15.30";  //$_GET[officeHours]
    echo "Mit navn er " . $firstName . " " . $lastName . ". Jeg kan træffes i kontortiden " . $officeHours . " på telefon " . $phoneNum .  " eller email " . $email;

?>